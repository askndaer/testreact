import {
  ADD_LIST_SUCCESS, DELETE_LIST_SUCCESS, EDIT_LIST_SUCCESS, FETCH_LISTS_SUCCESS, GET_EDIT_LIST_ID, MARK_COMPLETE
} from "../constants/action-types";

const initState = {
  lists: [],
  editId: null
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_LISTS_SUCCESS:
      console.log("reducer FETCH_LISTS_SUCCESS", action.payload.lists);
      return {
        ...state,
        lists: [...state.lists, ...action.payload.lists]
      };

    case ADD_LIST_SUCCESS:
      console.log("reducer ADD_LIST_SUCCESS", action.payload.list);
      return {
        ...state,
        lists: [...state.lists, action.payload.list]
      };
    case GET_EDIT_LIST_ID:
      console.log("reducer GET_EDIT_LIST_ID", action.payload.id);
      return {
        ...state,
        editId: action.payload.id
      };
    case EDIT_LIST_SUCCESS:
      const newLists = state.lists.map(item =>
        item.id === action.payload.list.id ? action.payload.list : item
      );
      console.log("reducer EDIT_LIST_SUCCESS", newLists);
      return {
        ...state,
        lists: [...newLists],
        editId: null
      };
    case MARK_COMPLETE:
      const newLists1 = state.lists.map(item =>
        item.id === action.payload.id ? { ...item, completed: !item?.completed } : item
      );
      console.log("reducer EDIT_LIST_SUCCESS", newLists1);
      return {
        ...state,
        lists: [...newLists1],
        editId: null
      };
    case DELETE_LIST_SUCCESS:
      console.log("reducer DELETE_LIST_SUCCESS", action.payload.id);
      const newList = state.lists.filter(list => list.id !== action.payload.id);

      return {
        ...state,
        lists: [...newList]
      };

    default:
      return { ...state };
  }
};

export default reducer;
