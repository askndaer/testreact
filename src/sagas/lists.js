import { call, put, takeEvery } from "redux-saga/effects";
import {
  ADD_LIST, ADD_LIST_SUCCESS, DELETE_LIST, DELETE_LIST_SUCCESS, EDIT_LIST, EDIT_LIST_SUCCESS, FETCH_LISTS, FETCH_LISTS_SUCCESS
} from "../constants/action-types";

const BASE_URL = "http://127.0.0.1:5000";
const headers = { "Content-Type": "application/json" };

function* fetchLists() {
  const lists = yield call(() =>
    fetch(`${BASE_URL}/lists`, {
      method: "GET"
    }).then(response => response.json())
  );
  yield put({ type: FETCH_LISTS_SUCCESS, payload: { lists } });
}

function* addList({ payload }) {

  const list = yield call(() =>
    fetch(`${BASE_URL}/lists`, {
      headers,
      body: JSON.stringify(payload.list),
      method: "POST"
    }).then(response => response.json())
  );
  console.log("saga ADD_LIST_SUCCESS", { list });
  yield put({ type: ADD_LIST_SUCCESS, payload: { list } });
}

function* editTodo({ payload }) {
  const id = payload.list.id;
  const list = yield call(() =>
    fetch(`${BASE_URL}/lists/${id}`, {
      headers,
      body: JSON.stringify(payload.list),
      method: "PUT"
    }).then(response => response.json())
  );
  console.log("saga EDIT_LIST_SUCCESS", { list });
  yield put({ type: EDIT_LIST_SUCCESS, payload: { list } });
}

function* deleteList({ payload }) {
  const id = payload.id;
  yield call(() =>
    fetch(`${BASE_URL}/lists/${id}`, {
      method: "DELETE"
    }).then(response => response.json())
  );
  console.log("saga DELETE_LIST_SUCCESS", id);
  yield put({ type: DELETE_LIST_SUCCESS, payload: { id } });
}

function* watchFetchLists() {
  yield takeEvery(FETCH_LISTS, fetchLists);
}
function* watchAddTodo() {
  yield takeEvery(ADD_LIST, addList);
}
function* watchEditTodo() {
  yield takeEvery(EDIT_LIST, editTodo);
}
function* watchDeleteTodo() {
  yield takeEvery(DELETE_LIST, deleteList);
}

export default [
  watchFetchLists(),
  watchAddTodo(),
  watchEditTodo(),
  watchDeleteTodo()
];
