
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { addList } from "../actions";

import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

const Add = () => {


  // const { register, errors, handleSubmit, reset } = useForm();
  const { control, register, handleSubmit, watch, formState: { errors }, reset } = useForm();

  const dispatch = useDispatch();

  const postList = data => {
    const newList = {
      ...data,
      completed: false
    };
    dispatch(addList(newList));
    reset();
  };

  function RedBar() {
    return (
      <Box
        sx={{
          height: 10,
        }}
      />
    );
  }

  return (
    <React.Fragment>
      <div
        component="form"
        sx={{
          p: 2,
          display: 'flex',
          flexDirection: 'column',

        }}
        onSubmit={handleSubmit(postList)}
      >


        <TextField fullWidth name="title" id="outlined-basic" label="Title" variant="outlined"
          required={errors.title}
          {...register("title", { required: true })}
        />
        <RedBar />
        <TextField fullWidth name="note" id="outlined-basic" label="Note" variant="outlined"
          required={errors.note}
          {...register("note", { required: true })}
        />
        <RedBar />
        <Controller
          control={control}
          name='date'
          style={{ width: '100%' }}
          fullWidth
          render={({ field }) => (
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                style={{ width: '100%' }}
                fullWidth
                label="Date desktop"
                inputFormat="MM/DD/YYYY"
                value={field.value}
                onChange={(date) => field.onChange(date)}
                selected={field.value}
                renderInput={(params) => <TextField {...params} />}
                {...register("date", { required: true })}
              />
            </LocalizationProvider>

          )}
        />

        <RedBar />


        <Button fullWidth variant="contained" type="submit">Add</Button>

      </div>
    </React.Fragment>
  );
};

export default Add;
