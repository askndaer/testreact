import { Container } from '@mui/material';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import React from "react";
import ItemList from './ItemList';

const Main = () => {
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));
  return (
    <Container fixed>


      <Box sx={{ flexGrow: 1 }} >

        <div sx={{ minWidth: 275 }}>
          <ItemList />
        </div>
      </Box>

    </Container >
  );

};

export default Main;
