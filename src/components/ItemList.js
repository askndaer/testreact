import { Grid, TextField } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchLists } from "../actions";
import Add from './Add';
import Item from "./Item";
const ItemList = () => {
    const lists = useSelector(state => state.lists);
    const [FilterBy, setFilterBy] = useState("ALL")
    const [SearchWord, setSearchWord] = useState("")
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchLists());
    }, [dispatch]);


    const buttons = [
        <Button key="ALL" onClick={() => setFilterBy("ALL")}>ALL</Button>,
        <Button key="REMAINING" onClick={() => setFilterBy("REMAINING")}>REMAINING</Button>,
        <Button key="COMPLETED" onClick={() => setFilterBy("COMPLETED")}>COMPLETED</Button>,
    ];

    const renderLists = () => {
        const FilteredList1 = _.filter(
            lists, function (o) {
                if (o.title.includes(SearchWord)) return true

            })


        const FilteredList = _.filter(
            FilteredList1, function (o) {
                if (FilterBy == "COMPLETED") return o.completed
                if (FilterBy == "REMAINING") return !o.completed
                return true;
            })

        const SortedList = _.orderBy(FilteredList, ['date', 'id'], ['desc', 'desc']);
        return SortedList?.map(list => <Item key={list.id} list={list} />) ?? <p>loading...</p>
    }

    return (
        <>
            <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                <Grid container spacing={3}>
                    {/* Chart */}
                    <Grid item xs={12} md={8} lg={9}>
                        <Paper
                            sx={{
                                p: 2,
                                display: 'flex',
                                flexDirection: 'column',
                                height: 240,
                            }}
                        >

                            <Add />

                        </Paper>
                    </Grid>

                    <Grid item xs={12} md={4} lg={3}>
                        <Paper
                            sx={{
                                p: 2,
                                display: 'flex',
                                flexDirection: 'column',
                                height: 240,
                            }}
                        >

                            <ButtonGroup
                                //    variant="contained"
                                orientation="vertical"
                                size="large" aria-label="large button group">
                                {buttons}
                            </ButtonGroup>
                            <Box
                                sx={{
                                    height: 10,
                                }}
                            />
                            <TextField label="Search" id="outlined-basic" variant="outlined"
                                value={SearchWord} onChange={(e) => {
                                    setSearchWord(e.target.value)
                                }}
                            />

                        </Paper>
                    </Grid>

                </Grid>
                <Grid item xs={12} md={4} lg={3} marginTop={2}>
                    {renderLists()}
                </Grid>

            </Container>

        </>
    );
};

export default ItemList;
