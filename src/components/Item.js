import { Card, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteList, editList, getEditListId, markComplete } from "../actions";
const Item = ({ list }) => {
  const editId = useSelector(state => state.editId);
  const dispatch = useDispatch();



  const EditBtn = () => {
    const editId = useSelector(state => state.editId);
    const dispatch = useDispatch();
    return list.id !== editId ? (
      <button onClick={() => dispatch(getEditListId(list.id))}>edit</button>
    ) : (
      <button
        onClick={() =>
          dispatch(
            editList({
              name: "QQ",
              price: 123,
              notes: "qq QQ qq",
              id: 2
            })
          )
        }
      >
        confirm
      </button>
    );
  };

  return (
    <Box className={`item ${list.id === editId && "edit"}`} marginBottom={2} >
      <Card marginBottom={2} style={{ padding: 10 }}>

        <Typography
          sx={{ mt: 0.5, ml: 2 }}
          color="text.secondary"
          display="block"
          noWrap={true}
          style={list?.completed ? { textDecoration: "line-through" } : {}}
          textDecoration={'line-through'}
          variant="subtitle1"
        >{list.title}</Typography>
        <Typography
          sx={{ mt: 0.5, ml: 2 }}
          color="text.secondary"
          display="block"
          variant="subtitle2"
          style={list?.completed ? { textDecoration: "line-through" } : {}}
        >{list.note}</Typography>
        <Typography
          sx={{ mt: 0.5, ml: 2 }}
          color="text.secondary"
          display="block"
          variant="caption"
          style={list?.completed ? { textDecoration: "line-through" } : {}}
        >{list.date}</Typography>


        <button onClick={() => dispatch(deleteList(list.id))}>delete</button>
        <button onClick={() => dispatch(markComplete(list.id
        ))}>{list?.completed ? "Complete" : "uncompleted"}</button>


      </Card >
    </Box>

  );
};

export default Item;
