import {
  ADD_LIST, DELETE_LIST, EDIT_LIST, FETCH_LISTS, GET_EDIT_LIST_ID, MARK_COMPLETE
} from "../constants/action-types";

export const fetchLists = () => ({
  type: FETCH_LISTS
});
export const addList = list => ({
  type: ADD_LIST,
  payload: { list }
});
export const getEditListId = id => ({
  type: GET_EDIT_LIST_ID,
  payload: { id }
});
export const editList = list => ({
  type: EDIT_LIST,
  payload: { list }
});
export const deleteList = id => ({
  type: DELETE_LIST,
  payload: { id }
});


export const markComplete = id => ({
  type: MARK_COMPLETE,
  payload: { id }
});

